%include "lib.inc"
extern string_equals
global find_word

%define POINTER_SIZE    8   ;bytes

section .text

find_word:
    .loop:
        cmp    rsi, 0        ; empty str clause
        jz      .end
        push    rdi
        push    rsi             ; LinkedList.adress
        add     rsi, POINTER_SIZE
        call    string_equals
        test    rax, rax
        pop     rsi
        pop     rdi
        jnz     .end
        mov     rsi, [rsi]
        jmp     .loop
    .end:
        mov rax, rsi            ; rax points to entry adress
        ret
