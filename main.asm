%define LEGALLN 255
%include "lib.inc"
%include "dict.inc"
%include "words.inc"
section .bss
buffer:
    resb LEGALLN
section .rodata
msg_illegal_str: 
    db 'Error: non-legal string length', 0
msg_key_404: 
    db 'Error: key not found', 0

section .text
global _start
_start:
    mov rsi, LEGALLN
    mov rdi, buffer
    push rdi
    call read_word
    pop rdi
    cmp rax, 0
    je .illegal_str
    mov rsi, DICT_ELE
    call find_word
    cmp rax, 0
    je .key_not_found
    add rax, 8
    push rax
    mov rdi, rax
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    call print_newline
    xor rdi, rdi
    call exit
	.illegal_str:
        mov rdi, msg_illegal_str
        call print_error
        call print_newline
        jmp .end

	.key_not_found:
        mov rdi, msg_key_404
        call print_error
        call print_newline
        jmp .end

    .end:
        call exit