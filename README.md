# Assignment №2: Dictionary in assembly
---
Laboratory work No. 2: Dictionary in assembler


# Preparation

* Read the first chapters 3,4,5 "Low-level programming: C, assembly and program execution".

On defense, we can discuss the compilation cycle, the role of the linker, the preprocessor, the virtual memory device and the relationship between sections, segments, and memory regions. We can also talk about protection rings and privileged mode.


## Linked list

A linked list &mdash; is a data structure. An empty list is a null pointer; a non-empty list is a pointer to the first element of the list.
Each element contains data and a pointer to the next element.


Here is an example of a connected list (100, 200, 300).
Its beginning can be found by the pointer `x1`:

```nasm
section .data

x1:
dq x2
dq 100

x2:
dq x3
dq 200

x3:
dq 0
dq 300
```

There is often a need to store a data set in some kind of container. With a container, we perform operations to access its elements, add an element to the beginning or end, or to an arbitrary position, sort.

Different containers make some of these operations easy and fast, while others are &mdash; slow.
For example, it is inconvenient to add elements to an array, but you can quickly refer to an existing index.
In a linked list, on the contrary, it is convenient to add elements to any place, but index access is more difficult &mdash;- you need to view the entire list from the very beginning.

## Task

It is necessary to implement an assembler dictionary in the form of a connected list.
Each occurrence contains the address of the next pair in the dictionary, a key and a value.
Keys and values &mdash; addresses of null-terminated strings.

The dictionary is set statically, each new element is added to its beginning.
Using macros, we automate this process so that by specifying a new element using a new language construct, it will automatically be added to the beginning of the list, and the pointer to the beginning of the list will be updated. This way we won't need to manually maintain the correct links in the list.

Create a macro `colon` with two arguments: a key and a label that will be mapped to the value.
This label cannot be generated from the value itself, since there may be characters in the line that cannot occur in labels, for example, arithmetic signs, punctuation marks, etc. After using such a macro, you can directly specify the value associated with the key. Usage example:

```nasm
section .data

colon "third word", third_word
db "third word explanation", 0

colon "second word", second_word
db "second word explanation", 0

colon "first word", first_word
db "first word explanation", 0
```


The following files must be provided in the implementation:

- `main.asm`
- `lib.asm`
- `dict.asm`
- `colon.inc`

### Instructions

- Design the functions that you implemented in the first laboratory work as a separate library `lib.o`.

Don't forget to make all function names global labels.

- Create a file `colon.inc` and define a macro in it to create words in the dictionary.

The macro accepts two parameters:
- The key (in quotes)
- The name of the label on which the value will be located.

- In the `dict.asm` file, create the `find_word` function. It takes two arguments:
- A pointer to a null-terminated string.
- Pointer to the beginning of the dictionary.

`find_word` will go through the entire dictionary in search of a suitable key. If a suitable occurrence is found, it will return the address *of the beginning of the occurrence in the dictionary* (not values), otherwise it will return 0.

- The `words.inc` file should store the words defined using the 'colon` macro. Include this file in `main.asm'.
- In `main.asm` define the `_start` function, which:

- Reads a string of no more than 255 characters into the buffer with `stdin`.
- Tries to find an occurrence in the dictionary; if it is found, prints the value of this key in `stdout`. Otherwise, it gives an error message.

Don't forget that error messages should be output to `stderr`.

- Be sure to provide a `Makefile`.