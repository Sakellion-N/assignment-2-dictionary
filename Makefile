.PHONY: clean

main: main.o dict.o lib.o
	ld -o $@ $^
main.o: main.asm
	nasm -f elf64 -o $@ $<
main.asm: words.inc lib.inc dict.inc

%.o: %.asm
	nasm -f elf64 -o $@ $<

clean: 
	$(RM) *.o ./main
